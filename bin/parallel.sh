#!/bin/bash
echo Hello
sleep 5
echo I am job $CI_NODE_INDEX out of $CI_NODE_TOTAL

# Test if we are doing the "stage" exercise
if [ x$CI_JOB_STAGE != "xwork" ] ; then exit 0 ; fi


if [ -f node_${CI_NODE_INDEX}_data.md ] ; then
	echo "Found my file"
	cat node_${CI_NODE_INDEX}_data.md  
else
	echo "No file for me"
	exit 1
fi
