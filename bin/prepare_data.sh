#!/bin/bash
for i in {1..5}; do
	if [ -f node_${i}_data.md ] ; then
		echo "Cache for node $i found, I'm doing nothing"
	else
		echo "Data generated by commit $CI_COMMIT_SHORT_SHA for node $i"
		echo "Data generated by commit $CI_COMMIT_SHORT_SHA for node $i" > node_${i}_data.md
	fi
done
